package com.main;

/*I have created a class Calculator inside a package name com.hcl 
package com.hcl; 
public class Calculator { 
public int add(int a, int b){ 
return a+b; 
 } 
} 
how to use add method from another package. */
import com.model.Calculator;

public class CalculatorMain {
	public static void main(String[] args) {
		Calculator calculator = new Calculator();
		System.out.println("Addition of two numbers:" + calculator.add(6, 9));

	}
}
