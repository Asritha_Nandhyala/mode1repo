package com.main;

import com.model.Ascending;

public class AscendingMain {

	public static void main(String[] args) {

		int[] array = new int[] { 6, 2, 45, 86, 9 };
		System.out.println("Elements of given Array");

		Ascending.printArray(array);
		System.out.println("Elements of array sorted in ascending order: ");
		Ascending.sortArray(array);
		array = null;
	}

}