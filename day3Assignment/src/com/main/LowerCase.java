package com.main;
/*Write a Java program to convert all the characters in a string to lowercase. 
*/


public class LowerCase {

	public static void main(String[] args) {
		String str = "Welcome to Java Programing";

        
        String lowerStr = str.toLowerCase();

        
        System.out.println("Original String: " + str);
        System.out.println("String in lowercase: " + lowerStr);
		

	}

}
