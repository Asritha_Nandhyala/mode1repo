package com.hcl.main;

import com.hcl.model.Smallest;

public class SmallestMain {

	public static void main(String[] args) {
		int small = Smallest.findSmallest(67, 56, 75);
		System.out.println(small);
	}

}
