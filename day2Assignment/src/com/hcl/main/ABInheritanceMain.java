package com.hcl.main;

import com.hcl.model.B;

public class ABInheritanceMain {

	public static void main(String[] args) {
		B b = new B();
		b.setEmpno(123);

		b.setEmpname("Asritha");

		System.out.println(b.getEmpno());
		System.out.println(b.getEmpname());
		b.setSalary(456.6f);
		System.out.println(b.getSalary());

	}

}
