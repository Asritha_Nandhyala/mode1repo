package com.hcl.model;

public class Room {
	String roomNumber;
	String roomtype;
	String roomArea;
	String acmachine;

	public Room() {
		super();

	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public String setRoomNumber(String roomNumber) {
		return this.roomNumber = roomNumber;
	}

	public String getRoomtype() {
		return roomtype;
	}

	public String setRoomtype(String roomtype) {
		return this.roomtype = roomtype;
	}

	public String getRoomArea() {
		return roomArea;
	}

	public String setRoomArea(String roomArea) {
		return this.roomArea = roomArea;
	}

	public String getAcmachine() {
		return acmachine;
	}

	public String setAcmachine(String acmachine) {
		return this.acmachine = acmachine;
	}

	public Room(String roomNumber, String roomtype, String roomArea, String acmachine) {
		super();
		this.roomNumber = roomNumber;
		this.roomtype = roomtype;
		this.roomArea = roomArea;
		this.acmachine = acmachine;
	}

}
