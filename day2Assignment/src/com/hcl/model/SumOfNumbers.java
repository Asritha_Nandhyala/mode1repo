package com.hcl.model;
/*Your task is to create the class Addition and the required methods 
 * so that the code prints  the sum of the numbers passed to the function addition. */
public class SumOfNumbers {
	
		public int add(int[] numbers)
	    {
	        int sum = 0;
	        for(int num:numbers)
	        {
	            if(sum !=0){
	                System.out.print("+");
	            }
	            sum+=num;
	            System.out.print(num);
	        }
	        System.out.println("="+sum);
			return sum;
	    }
	}


