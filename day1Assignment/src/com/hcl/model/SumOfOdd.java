package com.hcl.model;

public class SumOfOdd {

	public int checkSum(int number) {

		int number1;
		int temp = 0;
		int sum = 0;

		number1 = number % 10;
		if (number1 % 2 != 0) {
			sum = sum + number1;

		} else {
			number = number / 10;
		}

		if (sum % 2 == 0) {
			temp = -1;
		} else {
			temp = 1;
		}

		return temp;
	}
}
