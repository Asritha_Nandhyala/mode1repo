package com.hcl.model;
/*Write a program to read a number, 
 * calculate the sum of squares of even digits (values) present in the given number. */

public class SquareSum {
	public static int sumOfSuaresOfEvenDigits(int number) {

		int n1;
		int sum = 0;

		while (number > 0) {
			n1 = number % 10;
			if ((n1 % 2) == 0)
				sum += n1 * n1;
			number /= 10;

		}

		return sum;
	}
}
